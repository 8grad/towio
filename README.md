# Kirby-TailwindCSS-Mix
### Boilerplate for Kirby CMS &amp; Laravel Mix &amp; Tailwind CSS

Install frontend
- [Laravel Mix](https://laravel-mix.com  ) | https://github.com/laravel-mix/laravel-mix#readme
- [Tailwind CSS](https://tailwindcss.com ) | https://github.com/tailwindlabs/tailwindcss
- [BrowserSync](https://browsersync.io/)
```
$ nvm use
$ npm i |install
```

Install backend
- [Kirby CMS](https://getkirby.com  )
```
cd public
composer install
```

Update backend

```
cd public
composer update
```

---
#DEV

watch
```
npm run watch
```
production
```
npm run prod
```

---
© [Marc Wright](https://marcwright.de) March 2022
