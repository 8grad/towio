<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-main',
    'version' => 'dev-main',
    'aliases' => 
    array (
    ),
    'reference' => 'fc28d8e2d2cd1b0eba7d353a3303877c18deb3a1',
    'name' => 'getkirby/starterkit',
  ),
  'versions' => 
  array (
    'claviska/simpleimage' => 
    array (
      'pretty_version' => '3.6.5',
      'version' => '3.6.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '00f90662686696b9b7157dbb176183aabe89700f',
    ),
    'filp/whoops' => 
    array (
      'pretty_version' => '2.14.5',
      'version' => '2.14.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a63e5e8f26ebbebf8ed3c5c691637325512eb0dc',
    ),
    'getkirby/cms' => 
    array (
      'pretty_version' => '3.6.2',
      'version' => '3.6.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '04127160ed1ab8dc277763cd8ba4ebc5a097bc18',
    ),
    'getkirby/composer-installer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c98ece30bfba45be7ce457e1102d1b169d922f3d',
    ),
    'getkirby/starterkit' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
      ),
      'reference' => 'fc28d8e2d2cd1b0eba7d353a3303877c18deb3a1',
    ),
    'laminas/laminas-escaper' => 
    array (
      'pretty_version' => '2.9.0',
      'version' => '2.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '891ad70986729e20ed2e86355fcf93c9dc238a5f',
    ),
    'league/color-extractor' => 
    array (
      'pretty_version' => '0.3.2',
      'version' => '0.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '837086ec60f50c84c611c613963e4ad2e2aec806',
    ),
    'matthecat/colorextractor' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'michelf/php-smartypants' => 
    array (
      'pretty_version' => '1.8.1',
      'version' => '1.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '47d17c90a4dfd0ccf1f87e25c65e6c8012415aad',
    ),
    'phpmailer/phpmailer' => 
    array (
      'pretty_version' => 'v6.5.3',
      'version' => '6.5.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'baeb7cde6b60b1286912690ab0693c7789a31e71',
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.24.0',
      'version' => '1.24.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '749045c69efb97c70d25d7463abba812e91f3a44',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.24.0',
      'version' => '1.24.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8590a5f561694770bdcd3f9b5c69dde6945028e8',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.24.0',
      'version' => '1.24.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0abb51d2f102e00a4eefcf46ba7fec406d245825',
    ),
    'symfony/polyfill-php72' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
  ),
);
