<?php
  /*
    Snippets are a great way to store code snippets for reuse
    or to keep your templates clean.

    This intro snippet is reused in multiple templates.
    While it does not contain much code, it helps to keep your
    code DRY and thus facilitate maintenance when you have
    to make changes.

    More about snippets:
    https://getkirby.com/docs/guide/templates/snippets
  */
?>
<header class="h1">
  h1<h1><?= $page->headline()->or($page->title())->html() ?></h1>
  <?php if ($page->subheadline()->isNotEmpty()): ?>
    h2<h2><?= $page->subheadline()->html() ?></h2>
    h3<h3>Egal welches Handwerk, egal an welchem Arbeitslatz: </h3>
    h3<h3 class="font-bold">Mit towio sparen Sie Zeit, Nerven und Geld. Ganz einfach. Für das ganze Team.</h3>
    h4<h4>Auf der Baustelle lorem ipsum
      Lorem ipsum</h4>

    p<p>Niemand soll hier die Katze im Sack kaufen. Wenn Sie sich einfach mal durchklicken, ein paar Funktionalitäten
      testen oder auch mal mit einem Ihrer Mitarbeiter towios Möglichkeiten auschecken möchten - dann hauen Sie rein,
      all das kostet Sie nichts.
      Lorem ipsum dolor sit amet</p>

    p
    <p>
      Niemand soll hier die Katze im Sack <a href="#">kaufen</a>.
    </p>

    ol
    <ol class="list-decimal list-inside mb-4">
      <li>erstens</li>
      <li>zweitens</li>
    </ol>

    ul
    <ul class="list-disc list-inside mb-4">
      <li>erstens</li>
      <li>zweitens</li>
    </ul>

    <a class="button" href="#">Schreiben sie uns</a>


  <?php endif ?>
</header>
