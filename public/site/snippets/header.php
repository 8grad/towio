<?php
  /*
    Snippets are a great way to store code snippets for reuse
    or to keep your templates clean.

    This header snippet is reused in all templates.
    It fetches information from the `site.txt` content file
    and contains the site navigation.

    More about snippets:
    https://getkirby.com/docs/guide/templates/snippets
  */
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">

  <?php
    /*
      In the title tag we show the title of our
      site and the title of the current page
    */
  ?>
  <title><?= $site->title() ?> | <?= $page->title() ?></title>

  <?php
    /*
      Stylesheets can be included using the `css()` helper.
      Kirby also provides the `js()` helper to include script file.
      More Kirby helpers: https://getkirby.com/docs/reference/templates/helpers
    */
  ?>
  <!--  --><? //= css([
    //    'assets/css/prism.css',
    //    'assets/css/lightbox.css',
    //    'assets/css/index.css',
    //    '@auto'
    //  ]) ?>
  <?= css(['assets/css/app.css?v=' . ($kirby->option('debug') ? time() : 1)]) ?>
  <?= js('assets/js/app.js?v=' . ($kirby->option('debug') ? time() : 1), ['defer' => true]) ?>


  <?php
    /*
      The `url()` helper is a great way to create reliable
      absolute URLs in Kirby that always start with the
      base URL of your site.
    */
  ?>
  <link rel="shortcut icon" type="image/x-icon" href="<?= url('favicon.ico') ?>">
</head>
<body>

<header class="header border border-dashed border-red mb-32 py-8">
  <?php
    /*
      We use `$site->url()` to create a link back to the homepage
      for the logo and `$site->title()` as a temporary logo. You
      probably want to replace this with an SVG.
    */
  ?>
  <h1>
    <a class='text-red font-bold no-underline' href="<?= $site->url() ?>">
      <?= $site->title()->html() ?>
    </a>

  </h1>
  <nav class="menu">
    <?php
      /*
        In the menu, we only fetch listed pages,
        i.e. the pages that have a prepended number
        in their foldername.

        We do not want to display links to unlisted
        `error`, `home`, or `sandbox` pages.

        More about page status:
        https://getkirby.com/docs/reference/panel/blueprints/page#statuses
      */
    ?>
    <?php foreach ($site->children()->listed() as $item): ?>
      <a <?php e($item->isOpen(), 'aria-current ') ?> href="<?= $item->url() ?>"><?= $item->title()->html() ?></a>
    <?php endforeach ?>
    <?php snippet('social') ?>

    <ul class="grid grid-flow-col gap-4 text-center font-bold">
      <li class="bg-red rounded-md">red</li>
      <li class="bg-blue rounded-md">blue</li>
      <li class="bg-gray rounded-md">gray</li>
      <li class="bg-light-gray rounded-md">light gray</li>
      <li class="bg-white rounded-md border border-light-gray">white</li>
    </ul>

  </nav>
</header>

<main class="main">
