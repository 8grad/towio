module.exports = {
  // Purge files
  content: [
    './public/site/controllers/**/*.php',
    './public/site/plugins/**/*.php',
    './public/site/snippets/**/*.php',
    './public/site/templates/**/*.php',
    './src/scripts/**/*.js',
  ],
  safelist: ['is-selected', 'is-open'],
  darkMode: 'media',

  theme: {
    fontFamily: {
      sans: ['Aeonik', 'Helvetica', 'Roboto', 'sans-serif'],
    },
    screens: {
      'sm': '640px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1440px',
    },
    container: {
      padding: {
        DEFAULT: '0.75rem',
        md: '1.5rem',
      },
    },
    fontSize: {
      sm: ['14px', '20px'],
      base: ['16px', '20px'],
      md: ['18px', '22px'],
      lg: ['21px', '28px'],
      xl: ['24px', '32px'],
      '2xl': ['30px', '36px'],
      '3xl': ['32px', '38px'],
      '4xl': ['36px', '32px'],
      '5xl': ['40px', '48px'],
      '6xl': ['48px', '32px'],
      '7xl': ['60px', '72px'],
    },
    extend: {
      colors: {
        red: '#CC0100',
        primary: '#CC0000',
        blue: '#009FE3',
        secondary: '#009FE3',
        white: '#ffffff',
        gray: '#757575',
        smoke: '#EBEBEB',
        "light-gray": '#BCBCBC',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
